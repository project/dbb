CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers


INTRODUCTION
------------

Drupal Bot Builder for Facebook Messenger
The webhook implementation to handle Facebook Messenger API calls


REQUIREMENTS
------------

This module requires (Paragraphs module](https://www.drupal.org/project/paragraphs).


INSTALLATION
------------

1. Install the Bot Builder module as you would normally install
any Drupal contrib module.
2.  Require dbb module with composer:
    `composer require drupal/dbb`
3. Visit https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
--------------

1. Navigate to Administration > Extend and enable the Bot Builder module.

2. Create new node of type chatbot_webhook.

3. Specify the canonical node url as webhook in facebook for messenger catbot application
     @see --> https://developers.facebook.com/apps/<your_app_id>/messenger/settings/


MAINTAINERS
-----------

The 1.1.x branch compatible with Drupal ^8||^9||^10 was created by:

 * Oleksii Bondarenko (alex.mazaltov) - https://www.drupal.org/u/alexmazaltov


LICENSE
-------

GPLv2 - GNU GENERAL PUBLIC LICENSE (Version 2, June 1991)
